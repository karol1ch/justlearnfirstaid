from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.views.generic import TemplateView


class HomePageView(TemplateView):
    template_name = 'home.html'


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was updated.')
            return redirect('change_password')
        messages.error(request, 'Error')
        return redirect('change_password_error')
    else:
        form = PasswordChangeForm(request.user)
        return render(request, 'registration/change_password.html', {'form': form})


@login_required
def change_password_error(request):
    form = PasswordChangeForm(request.user, request.POST)
    return render(request, 'registration/change_password_error.html', {'user': request.user, 'form': form})
