from django.urls import path
from .views import HomePageView
from JustLearnFirstAid.home import views

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('password/', views.change_password, name='change_password'),
    path('password/fail/', views.change_password_error, name='change_password_error')
]
