from django.contrib import admin

from .models import TrainingCourse
from .models import Lesson
from .models import LessonItem
from .models import Exam
from .models import OpenQuestion

admin.site.register(TrainingCourse)
admin.site.register(Lesson)
admin.site.register(LessonItem)
admin.site.register(Exam)
admin.site.register(OpenQuestion)
