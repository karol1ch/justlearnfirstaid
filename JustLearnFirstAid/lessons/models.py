from django.db import models

from JustLearnFirstAid.home.models import User


class LessonItem(models.Model):
    """ type = [Text, Image, Video] """
    description = models.TextField(max_length=64)
    data = models.FileField()
    type = models.CharField(max_length=8)

    def __str__(self):
        return f"{self.type}: {self.description}"


class Lesson(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField()
    items = models.ManyToManyField(LessonItem)
    published_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-published_at']

    def __str__(self):
        return self.title


class OpenQuestion(models.Model):
    content = models.TextField()
    max_points = models.IntegerField()

    def __str__(self):
        return self.content[:50]


class Exam(models.Model):
    title = models.CharField(max_length=32)
    description = models.TextField(max_length=1024)
    open_questions = models.ManyToManyField(OpenQuestion)
    available_to = models.DateTimeField()

    def __str__(self):
        return self.title


class TrainingCourse(models.Model):
    name = models.CharField(max_length=32)
    description = models.TextField(max_length=256)
    students = models.ManyToManyField(User, blank=True)
    lessons = models.ManyToManyField(Lesson, blank=True)
    exams = models.ManyToManyField(Exam, blank=True)

    def __str__(self):
        return self.name


class ExamResult(models.Model):
    student = models.ForeignKey(User, on_delete=models.PROTECT)
    exam = models.ForeignKey(Exam, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.student.id_card_number}: {self.exam.title}'


class OpenQuestionAnswer(models.Model):
    question = models.ForeignKey(OpenQuestion, on_delete=models.PROTECT)
    answer = models.TextField()
    exam_result = models.ForeignKey(ExamResult, on_delete=models.PROTECT)
    points = models.IntegerField(null=True)

    def __str__(self):
        return f'[{self.points}/{self.question.max_points}] {self.question.content[:30]}'
