from django import forms
from django.utils.safestring import mark_safe


class ExamForm(forms.Form):
    def __init__(self, *args, **kwargs):
        questions = kwargs.pop('questions')
        super(ExamForm, self).__init__(*args, **kwargs)
        for i, question in enumerate(questions):
            label = mark_safe(f"{i + 1}. {question.content} [{question.max_points} pts]<br/>")
            self.fields[f'question_id:{question.id}'] = forms.CharField(label=label, widget=forms.Textarea)
