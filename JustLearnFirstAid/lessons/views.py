from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone

from JustLearnFirstAid.home.models import User
from .forms import ExamForm
from .models import Exam
from .models import Lesson, OpenQuestionAnswer, ExamResult
from .models import TrainingCourse


@login_required
def lesson_details(request, course_id, lesson_id):
    lesson = get_object_or_404(Lesson, pk=lesson_id)
    context = {
        'lesson': lesson,
        'course_id': course_id
    }
    return render(request, 'lessons/details.html', context)


@login_required
def all_training_courses(request):
    training_courses = TrainingCourse.objects.filter(students__id_card_number=request.user.id_card_number)
    paginator = Paginator(training_courses, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'page_obj': page_obj}
    return render(request, 'training_courses/index.html', context)


@login_required
def training_course_details(request, course_id):
    student_id = request.user.id_card_number
    training_course = get_object_or_404(TrainingCourse, pk=course_id)
    lessons = training_course.lessons.all()
    paginator_lessons = Paginator(lessons, 10)
    page_number_lessons = request.GET.get('page')
    page_obj_lessons = paginator_lessons.get_page(page_number_lessons)
    completed_exams = ExamResult.objects.filter(student__id_card_number=student_id)
    previous_exams = training_course.exams \
        .filter(Q(examresult__in=completed_exams) | Q(available_to__lte=timezone.now()))
    available_exams = training_course.exams.difference(previous_exams)
    context = {
        'course': training_course,
        'page_obj_lessons': page_obj_lessons,
        'available_exams': available_exams,
        'previous_exams': previous_exams
    }
    return render(request, 'training_courses/details.html', context)


@login_required
def get_exam(request, course_id, exam_id):
    exam = get_object_or_404(Exam, pk=exam_id)
    student_id = request.user.id_card_number
    if _exam_has_ended(exam, student_id):
        return HttpResponseRedirect(f'/courses/{course_id}/exams/{exam_id}/result')
    if request.method == 'POST':
        form = ExamForm(request.POST, questions=exam.open_questions.all())
        if form.is_valid():
            exam_result = _create_exam_result(student_id, exam_id)
            for field in form.fields:
                _create_question_answer(exam_result, field, form)
            return HttpResponseRedirect(f'/courses/{course_id}')
    else:
        form = ExamForm(questions=exam.open_questions.all())
    context = {
        'exam': exam,
        'form': form
    }
    return render(request, 'exams/exam.html', context)


@login_required
def get_exam_result(request, course_id, exam_id):
    try:
        student_id = request.user.id_card_number
        exam_result = ExamResult.objects.get(student__id_card_number=student_id, exam_id=exam_id)
        open_question_answers = OpenQuestionAnswer.objects.filter(exam_result=exam_result)
        points = sum(map(lambda answer: answer.points if answer.points else 0, open_question_answers))
        max_points = sum(map(lambda question: question.max_points, exam_result.exam.open_questions.all()))
        context = {
            'exam': exam_result.exam,
            'course_id': course_id,
            'open_question_answers': open_question_answers,
            'all_points': points,
            'max_points': max_points
        }
        return render(request, 'exams/result.html', context)
    except ExamResult.DoesNotExist:
        return HttpResponse('You have not submitted your exam in given time!')


def _create_question_answer(exam_result, field, form):
    if field.startswith('question_id:'):
        question_id = int(field.split(':')[1])
        OpenQuestionAnswer.objects.create(
            question_id=question_id,
            answer=form.cleaned_data[field],
            exam_result=exam_result
        )


def _create_exam_result(student_id, exam_id):
    student = get_object_or_404(User, id_card_number=student_id)
    return ExamResult.objects.create(
        student=student,
        exam_id=exam_id
    )


def _has_exam_expired(exam):
    return exam.available_to < timezone.now()


def _has_been_completed(exam, student_id):
    return ExamResult.objects \
        .filter(exam=exam) \
        .filter(student__id_card_number=student_id)


def _exam_has_ended(exam, student_id):
    return _has_exam_expired(exam) or _has_been_completed(exam, student_id)


def student_results(id_card_number, course_id):
    student_result = StudentResults(id_card_number, course_id)
    return student_result.find_all_results()


@login_required
def get_student_results(request, course_id):
    student_result = student_results(request.user.id_card_number, course_id)
    context = {
        'results': student_result,
        'course': get_object_or_404(TrainingCourse, pk=course_id),
        'user': request.user
    }
    return render(request, 'training_courses/user_result.html', context)


class StudentResults:
    def __init__(self, student_id, course_id):
        self.course_id = course_id
        self.student_id = student_id

    def find_all_results(self):
        training_course = get_object_or_404(TrainingCourse, pk=self.course_id)
        all_exams = ExamResult.objects.filter(student__id_card_number=self.student_id)
        completed_exams = training_course.exams.filter(examresult__in=all_exams)
        return {x: self.points_calculation(x.id) for x in list(completed_exams)}

    def points_calculation(self, exam_id):
        exam_result = ExamResult.objects.get(student__id_card_number=self.student_id, exam_id=exam_id)
        open_question_answers = OpenQuestionAnswer.objects.filter(exam_result=exam_result)
        points = sum(map(lambda answer: answer.points if answer.points else 0, open_question_answers))
        max_points = sum(map(lambda question: question.max_points, exam_result.exam.open_questions.all()))
        return points, max_points
