from django import template

register = template.Library()


@register.filter
def print_file_in_lines(file):
    try:
        return list(map(lambda line: line.decode('utf8'), file.readlines()))
    except IOError:
        return 'Unable to read content of the lesson! Please report it to the teacher!'
