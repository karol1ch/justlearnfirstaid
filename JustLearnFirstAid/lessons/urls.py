from django.urls import path

from . import views

urlpatterns = [
    path('<int:course_id>', views.training_course_details, name='courses'),
    path('<int:course_id>/lessons/<int:lesson_id>', views.lesson_details, name='courses'),
    path('<int:course_id>/exams/<int:exam_id>', views.get_exam, name='courses'),
    path('<int:course_id>/exams/<int:exam_id>/result', views.get_exam_result, name='courses'),
    path('<int:course_id>/results', views.get_student_results, name='courses'),
    path('', views.all_training_courses, name='courses')
]
