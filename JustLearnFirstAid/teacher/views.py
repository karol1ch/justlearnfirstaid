import csv
import re
import time
from collections import defaultdict
from datetime import date

from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect

from JustLearnFirstAid.home.models import User
from JustLearnFirstAid.lessons.models import TrainingCourse, Exam, ExamResult, OpenQuestionAnswer, Lesson
from JustLearnFirstAid.lessons.views import student_results
from JustLearnFirstAid.teacher.forms import ExamResultForm


def add_users_from_csv(request):
    data = csv.reader(open('./media/file.csv'), delimiter=",")
    pattern = re.compile(r"\W+")
    for row in data:
        if not User.objects.filter(email=row[0]).exists():
            user = User()
            user.email = row[0]
            user.first_name = row[1]
            user.last_name = row[2]
            user.id_card_number = row[3]
            temp_password = (User.objects.make_random_password())
            user.set_password(temp_password)
            today = date.today()
            t = time.localtime()
            current_time = time.strftime("%H:%M", t)
            user.save()
            with open('./media/passwords/' + pattern.sub("_", str(today) + str(current_time)), 'a') as file:
                writer = csv.writer(file)
                writer.writerow([user.id_card_number, temp_password])
        else:
            return redirect(add_users_from_csv_error)
    return render(request, 'courses/users_were_added.html')


def add_users_from_csv_error(request):
    return render(request, 'courses/users_added_error.html')


@staff_member_required
def lesson_details(request, course_id, lesson_id):
    lesson = get_object_or_404(Lesson, pk=lesson_id)
    context = {
        'lesson': lesson,
        'course_id': course_id
    }
    return render(request, 'lessons/details1.html', context)


@staff_member_required
def get_all_courses(request):
    training_courses = TrainingCourse.objects.all()
    paginator = Paginator(training_courses, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'page_obj': page_obj}
    return render(request, 'courses/all_courses.html', context)


@staff_member_required
def get_course_details(request, course_id):
    course = get_object_or_404(TrainingCourse, pk=course_id)
    context = {'course': course}
    return render(request, 'courses/details.html', context)


@staff_member_required
def get_exam_details(request, course_id, exam_id):
    course = get_object_or_404(TrainingCourse, pk=course_id)
    exam = get_object_or_404(Exam, pk=exam_id)
    max_points = sum(map(lambda question: question.max_points, exam.open_questions.all()))
    students = course.students.order_by('last_name', 'first_name')
    print(students)
    exam_results = ExamResult.objects \
        .filter(exam=exam) \
        .filter(student__in=students)

    print(ExamResult.objects.filter(exam=exam))
    exam_results_dict = {exam_result.student: exam_result for exam_result in exam_results}
    print('dict', exam_results_dict)
    points_dict = defaultdict(int, {exam_result.student: _get_exam_points(exam_result) for exam_result in exam_results})
    students_list = list(students)
    results = {student: (exam_results_dict.get(student), points_dict.get(student)) for student in students_list}
    context = {
        'exam': exam,
        'course_id': course_id,
        'results': results,
        'max_points': max_points
    }
    return render(request, 'exams/details.html', context)


def _set_points_to_answer(field, form):
    if field.startswith('answer_id:'):
        answer_id = int(field.split(':')[1])
        answer = OpenQuestionAnswer.objects.get(pk=answer_id)
        answer.points = form.cleaned_data[field]
        answer.save()


@staff_member_required
def get_all_students_results(request, course_id):
    training_course = get_object_or_404(TrainingCourse, pk=course_id)
    students = training_course.students.all()
    result_dict = {x: student_results(x.id_card_number, course_id) for x in students}
    context = {
        'course': training_course,
        'result_dict': result_dict
    }
    return render(request, 'courses/admin_points_view.html', context)


@staff_member_required
def get_exam_results(request, course_id, exam_id, student_id):
    try:
        exam_result = ExamResult.objects.get(exam_id=exam_id, student__id_card_number=student_id)
        answers = OpenQuestionAnswer.objects.filter(exam_result=exam_result)
        if request.method == 'POST':
            form = ExamResultForm(request.POST, answers=answers.all())
            if form.is_valid():
                for field in form.fields:
                    _set_points_to_answer(field, form)
                return HttpResponseRedirect(f'/teacher/courses/{course_id}/exams/{exam_id}')
        else:
            form = ExamResultForm(answers=answers)
        context = {
            'student': exam_result.student,
            'course_id': course_id,
            'exam': exam_result.exam,
            'form': form
        }
        return render(request, 'exams/results.html', context)
    except ExamResult.DoesNotExist:
        return HttpResponse('Result is not available! Student did not send the exam!')


def _get_exam_points(exam_result):
    open_question_answers = OpenQuestionAnswer.objects.filter(exam_result=exam_result)
    if any(filter(lambda answer: answer.points is None, open_question_answers)):
        return None
    return sum(map(lambda answer: answer.points if answer.points else 0, open_question_answers))
