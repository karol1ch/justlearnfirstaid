from django import forms
from django.utils.safestring import mark_safe
from django.core.validators import MaxValueValidator, MinValueValidator


class ExamResultForm(forms.Form):
    def __init__(self, *args, **kwargs):
        answers = kwargs.pop('answers')
        super(ExamResultForm, self).__init__(*args, **kwargs)
        for i, answer_obj in enumerate(answers):
            question = answer_obj.question.content
            answer = answer_obj.answer
            label = mark_safe(
                f"""{i + 1}. {question} [{answer_obj.question.max_points} pts]<br/>
                    <p>Answer: {answer}</p>"""
            )
            self.fields[f'answer_id:{answer_obj.id}'] = forms.IntegerField(label=label, initial=answer_obj.points,
                                                                           validators=[
                                                                               MinValueValidator(0),
                                                                               MaxValueValidator(
                                                                                   answer_obj.question.max_points)
                                                                           ])
