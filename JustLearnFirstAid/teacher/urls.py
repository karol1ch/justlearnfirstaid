from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_all_courses, name='teacher'),
    path('users_error', views.add_users_from_csv_error, name='add_users_from_csv_error'),
    path('users_added', views.add_users_from_csv, name='add_users_from_csv'),
    path('courses/<int:course_id>', views.get_course_details, name='teacher'),
    path('courses/<int:course_id>/results', views.get_all_students_results, name='teacher'),
    path('courses/<int:course_id>/exams/<int:exam_id>', views.get_exam_details, name='teacher'),
    path('courses/<int:course_id>/lessons/<int:lesson_id>', views.lesson_details, name='teacher'),
    path('courses/<int:course_id>/exams/<int:exam_id>/students/<slug:student_id>', views.get_exam_results,
         name='teacher')
]
